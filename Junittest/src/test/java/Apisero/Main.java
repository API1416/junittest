package Apisero;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.*;
import com.apisero.OddEven;
public class Main {
	
	static OddEven obj;
	@BeforeAll
	public static void abc1() {
		
		obj=new OddEven();
	System.out.println("i am BeforeAll");
	
}
	@BeforeEach
	public void abc2() {
		
	System.out.println("i am BeforeEach");
	
}
@Test
public void abc3() {
	
	assertEquals(obj.evenodd(10),true);
	System.out.println("i am TestCase");
	
}
@Test
public void abc4() {
	
	assertEquals(obj.evenodd(11),false);
	System.out.println("i am TestCase");
}
@AfterAll
public static void abc5() {
	System.out.println("i am AfterEach");
}
@AfterEach
public void abc6() {
	System.out.println("i am AfterAll");
}
	}


